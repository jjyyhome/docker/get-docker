#!/usr/bin/env bash
git submodule init


git submodule add https://github.com/kkoojjyy/docker-machine-kvm.git ../docker-machine-kvm

git submodule add https://github.com/kkoojjyy/docker-machine-vmwareworkstation.git ../docker-machine-vmareworkstation

git submodule add https://github.com/kkoojjyy/docker-android.git ../docker-android

git submodule add https://github.com/moby/moby.git  ../moby.git

git submodule add https://github.com/kkoojjyy/drone.git  ../drone

git submodule update


